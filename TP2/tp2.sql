-- Maxime Girard
-- GIRM30058500
-- INF3180 G
-- GROUPE 30
-- SESSION AUTOMNE 2012

PROMPT Creation des tables

SET ECHO ON
SET SERVEROUTPUT ON 
SPOOL tp2.out

DROP TABLE Inscription
/
DROP TABLE GroupeCours
/
DROP TABLE Prealable
/
DROP TABLE Cours
/
DROP TABLE SessionUQAM
/
DROP TABLE Etudiant
/
DROP TABLE Professeur
/
ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'
/

/**
* Contraintes C1 et C2
* C1 utilise REGEXP pour verifier le patron des caract�res. 
* C2 utilise CHECK pour verifier que nbCredit soit entre 0 - 99 avec la fonction BETWEEN ... AND ... 
*/

CREATE TABLE Cours
(sigle 		CHAR(7) 	NOT NULL,
 titre 		VARCHAR(50) 	NOT NULL,
 nbCredits 	INTEGER 	NOT NULL,
 CONSTRAINT ClePrimaireCours PRIMARY KEY (sigle),
 CONSTRAINT C1 CHECK (REGEXP_LIKE(SIGLE,'[a-Z][a-Z][a-Z][0-9][0-9][0-9][0-9]')),
 CONSTRAINT C2 CHECK (nbCredits BETWEEN 0 AND 99)
)
/

CREATE TABLE Prealable
(sigle 		CHAR(7) 	NOT NULL,
 siglePrealable CHAR(7) 	NOT NULL,
 CONSTRAINT ClePrimairePrealable PRIMARY KEY 	(sigle,siglePrealable),
 CONSTRAINT CEsigleRefCours FOREIGN KEY 	(sigle) REFERENCES Cours,
 CONSTRAINT CEsiglePrealableRefCours FOREIGN KEY 	(siglePrealable) REFERENCES Cours(sigle)
)
/

/**
* Contrainte C3 sous la forme d'un CHECK qui verifie les dates. 
*/

CREATE TABLE SessionUQAM
(codeSession 	INTEGER		NOT NULL,
 dateDebut 	DATE		NOT NULL,
 dateFin 	DATE		NOT NULL,
 CONSTRAINT ClePrimaireSessionUQAM PRIMARY KEY 	(codeSession),
 CONSTRAINT C3 CHECK(dateFin >= dateDebut + 90)
)
/

CREATE TABLE Professeur
(codeProfesseur		CHAR(5)	NOT NULL,
 nom			VARCHAR(10)	NOT NULL,
 prenom		VARCHAR(10)	NOT NULL,
 CONSTRAINT ClePrimaireProfesseur PRIMARY KEY 	(codeProfesseur)
)
/
CREATE TABLE GroupeCours
(sigle 		CHAR(7) 	NOT NULL,
 noGroupe	INTEGER		NOT NULL,
 codeSession	INTEGER		NOT NULL,
 maxInscriptions	INTEGER		NOT NULL,
 codeProfesseur		CHAR(5)	NOT NULL,
CONSTRAINT ClePrimaireGroupeCours PRIMARY KEY 	(sigle,noGroupe,codeSession),
CONSTRAINT CESigleGroupeRefCours FOREIGN KEY 	(sigle) REFERENCES Cours,
CONSTRAINT CECodeSessionRefSessionUQAM FOREIGN KEY 	(codeSession) REFERENCES SessionUQAM,
CONSTRAINT CEcodeProfRefProfesseur FOREIGN KEY(codeProfesseur) REFERENCES Professeur 
)
/
CREATE TABLE Etudiant
(codePermanent 	CHAR(12) 	NOT NULL,
 nom		VARCHAR(10)	NOT NULL,
 prenom		VARCHAR(10)	NOT NULL,
 codeProgramme	INTEGER,
CONSTRAINT ClePrimaireEtudiant PRIMARY KEY 	(codePermanent)
)
/

/**
* Table Inscription contient les contraintes C4 ET C5
* La contrainte C4 est implementer sous la forme d'un CHECK
* Verifie si une des deux valeur concernee est 'null'
* C5 est constitue de 'ON DELETE CASCADE' 
* C7 verifie la cote(si est nulle ou A,b,c,d,e)
*/

CREATE TABLE Inscription
(codePermanent 	CHAR(12) 	NOT NULL,
 sigle 		CHAR(7) 	NOT NULL,
 noGroupe	INTEGER		NOT NULL,
 codeSession	INTEGER		NOT NULL,
 dateInscription DATE		NOT NULL,
 dateAbandon	DATE,
 note		INTEGER,
 cote		CHAR(1), 
CONSTRAINT ClePrimaireInscription PRIMARY KEY 	(codePermanent,sigle,noGroupe,codeSession),
CONSTRAINT CERefGroupeCours FOREIGN KEY 	(sigle,noGroupe,codeSession) REFERENCES GroupeCours,
CONSTRAINT CECodePermamentRefEtudiant FOREIGN KEY (codePermanent) REFERENCES Etudiant(codePermanent) ON DELETE CASCADE,
CONSTRAINT C4 CHECK (dateAbandon IS NULL OR note IS NULL),
CONSTRAINT C7 CHECK (cote in ('A', 'B', 'C', 'D','E') or cote IS NULL)
)
/

/*
* Contrainte d'integrite 6 - C6 
* Sous forme d'un TRIGGER verifie que l'ECART entre la nouvelle note et l'ancienne 
* ne depasse pas 20 points. Utilisation de la valeur absolue de la difference des deux notes. 
* Si l'ecart est plus de 20 points, une exception est levee. 
*
*/

CREATE OR REPLACE TRIGGER calculerCote
BEFORE UPDATE OF note ON Inscription
FOR EACH ROW
WHEN (old.note != new.note)
DECLARE 
uneCote char(1);
uneNote number;
BEGIN
 uneNote:= :new.note; 
 uneCote := fCotePourNote(uneNote);
 UPDATE Inscription 
 SET cote = uneCote
 WHERE note = uneNote;
END;
/

CREATE OR REPLACE FUNCTION fCotePourNote (noteToConvert Inscription.note%TYPE)
RETURN Inscription.cote%TYPE IS coteConvertie Inscription.cote%TYPE;
BEGIN
IF (noteToConvert BETWEEN 0 AND 59) THEN coteConvertie:='E' ;
ELSIF (noteToConvert BETWEEN 60 AND 69) THEN coteConvertie:='D' ;
ELSIF (noteToConvert BETWEEN 70 AND 79) THEN coteConvertie:='C' ;
ELSIF (noteToConvert BETWEEN 80 AND 89) THEN coteConvertie:='B' ;
ELSIF (noteToConvert BETWEEN 90 AND 100) THEN coteConvertie:='A' ;
ELSIF (noteToConvert IS NULL) THEN coteConvertie:= NULL ;
END IF;
RETURN coteConvertie;
END;
/ 

/*
 * PROCEDURE PBULLETIN. 
 */
CREATE OR REPLACE PROCEDURE pBulletin (cPermanent Inscription.codePermanent%TYPE) 
IS

nomEtudiant varchar2(15);
prenomEtudiant varchar2(15);
sigleEtudiant varchar2(7);
noGroupeEtudiant number;
noSessionEtudiant number;
noteEtudiant number;
coteEtudiant char(1);

CURSOR curseur(cPermanent Inscription.codePermanent%TYPE) IS
SELECT  sigle,noGroupe, codeSession, note, cote
FROM Inscription
WHERE codePermanent = cPermanent;

BEGIN

DBMS_OUTPUT.PUT('CODE PERMANENT : ');
DBMS_OUTPUT.PUT_LINE(cPermanent);

select nom 
into nomEtudiant
from etudiant 
where codePermanent = cPermanent;

DBMS_OUTPUT.PUT('NOM : ');
DBMS_OUTPUT.PUT_LINE(nomEtudiant);

select prenom
into prenomEtudiant
from etudiant
where codePermanent = cPermanent;

DBMS_OUTPUT.PUT('PRENOM : ');
DBMS_OUTPUT.PUT_LINE(prenomEtudiant);

OPEN curseur(cPermanent);
LOOP

FETCH curseur INTO sigleEtudiant, noGroupeEtudiant, noSessionEtudiant, noteEtudiant, coteEtudiant;
EXIT WHEN curseur%NOTFOUND;

DBMS_OUTPUT.PUT('SIGLE : ');
DBMS_OUTPUT.PUT(sigleEtudiant);

DBMS_OUTPUT.PUT('NOGROUPE : ');
DBMS_OUTPUT.PUT_LINE(noGroupeEtudiant);

DBMS_OUTPUT.PUT('SESSION : ');
DBMS_OUTPUT.PUT_LINE(noSessionEtudiant);

DBMS_OUTPUT.PUT('NOTE : ');
DBMS_OUTPUT.PUT_LINE(noteEtudiant);

DBMS_OUTPUT.PUT('COTE : ');
DBMS_OUTPUT.PUT_LINE(coteEtudiant);

END LOOP;
CLOSE curseur;

END pBulletin;
/

/*
 * FIN PROCEDURE PBULLETIN
 */
 
CREATE OR REPLACE VIEW MoyenneParGroupe AS
SELECT DISTINCT Sigle, noGroupe, codeSession, avg(note) as moyenneNote
FROM Inscription
GROUP BY noGroupe,Sigle, codeSession
/
 
PROMPT Insertion de donnees pour les essais

INSERT INTO Cours 
VALUES('INF3180','Fichiers et bases de donnees',3)
/
INSERT INTO Cours 
VALUES('INF5180','Conception et exploitation d''une base de donnees',3)
/
INSERT INTO Cours 
VALUES('INF1110','Programmation I',3)
/
INSERT INTO Cours 
VALUES('INF1130','Mathematiques pour informaticien',3)
/
INSERT INTO Cours 
VALUES('INF2110','Programmation II',3)
/
INSERT INTO Cours 
VALUES('INF3123','Programmation objet',3)
/
INSERT INTO Cours 
VALUES('INF2160','Paradigmes de programmation',3)
/

INSERT INTO Prealable 
VALUES('INF2110','INF1110')
/
INSERT INTO Prealable 
VALUES('INF2160','INF1130')
/
INSERT INTO Prealable 
VALUES('INF2160','INF2110')
/
INSERT INTO Prealable 
VALUES('INF3180','INF2110')
/
INSERT INTO Prealable 
VALUES('INF3123','INF2110')
/
INSERT INTO Prealable 
VALUES('INF5180','INF3180')
/

INSERT INTO SessionUQAM
VALUES(32003,'3/09/2003','17/12/2003')
/
INSERT INTO SessionUQAM
VALUES(12004,'8/01/2004','2/05/2004')
/

INSERT INTO Professeur
VALUES('TREJ4','Tremblay','Jean')
/
INSERT INTO Professeur
VALUES('DEVL2','De Vinci','Leonard')
/
INSERT INTO Professeur
VALUES('PASB1','Pascal','Blaise')
/
INSERT INTO Professeur
VALUES('GOLA1','Goldberg','Adele')
/
INSERT INTO Professeur
VALUES('KNUD1','Knuth','Donald')
/
INSERT INTO Professeur
VALUES('GALE9','Galois','Evariste')
/
INSERT INTO Professeur
VALUES('CASI0','Casse','Illa')
/
INSERT INTO Professeur
VALUES('SAUV5','Sauve','Andre')
/

INSERT INTO GroupeCours
VALUES('INF1110',20,32003,100,'TREJ4')
/
INSERT INTO GroupeCours
VALUES('INF1110',30,32003,100,'PASB1')
/
INSERT INTO GroupeCours
VALUES('INF1130',10,32003,100,'PASB1')
/
INSERT INTO GroupeCours
VALUES('INF1130',30,32003,100,'GALE9')
/
INSERT INTO GroupeCours
VALUES('INF2110',10,32003,100,'TREJ4')
/
INSERT INTO GroupeCours
VALUES('INF3123',20,32003,50,'GOLA1')
/
INSERT INTO GroupeCours
VALUES('INF3123',30,32003,50,'GOLA1')
/
INSERT INTO GroupeCours
VALUES('INF3180',30,32003,50,'DEVL2')
/
INSERT INTO GroupeCours
VALUES('INF3180',40,32003,50,'DEVL2')
/
INSERT INTO GroupeCours
VALUES('INF5180',10,32003,50,'KNUD1')
/
INSERT INTO GroupeCours
VALUES('INF5180',40,32003,50,'KNUD1')
/
INSERT INTO GroupeCours
VALUES('INF1110',20,12004,100,'TREJ4')
/
INSERT INTO GroupeCours
VALUES('INF1110',30,12004,100,'TREJ4')
/
INSERT INTO GroupeCours
VALUES('INF2110',10,12004,100,'PASB1')
/
INSERT INTO GroupeCours
VALUES('INF2110',40,12004,100,'PASB1')
/
INSERT INTO GroupeCours
VALUES('INF3123',20,12004,50,'GOLA1')
/
INSERT INTO GroupeCours
VALUES('INF3123',30,12004,50,'GOLA1')
/
INSERT INTO GroupeCours
VALUES('INF3180',10,12004,50,'DEVL2')
/
INSERT INTO GroupeCours
VALUES('INF3180',30,12004,50,'DEVL2')
/
INSERT INTO GroupeCours
VALUES('INF5180',10,12004,50,'DEVL2')
/
INSERT INTO GroupeCours
VALUES('INF5180',40,12004,50,'GALE9')
/

INSERT INTO Etudiant
VALUES('TREJ18088001','Tremblay','Jean',7416)
/
INSERT INTO Etudiant
VALUES('TREL14027801','Tremblay','Lucie',7416)
/
INSERT INTO Etudiant
VALUES('DEGE10027801','Degas','Edgar',7416)
/
INSERT INTO Etudiant
VALUES('MONC05127201','Monet','Claude',7316)
/
INSERT INTO Etudiant
VALUES('VANV05127201','Van Gogh','Vincent',7316)
/
INSERT INTO Etudiant
VALUES('MARA25087501','Marshall','Amanda',null)
/
INSERT INTO Etudiant
VALUES('STEG03106901','Stephani','Gwen',7416)
/
INSERT INTO Etudiant
VALUES('EMEK10106501','Emerson','Keith',7416)
/
INSERT INTO Etudiant
VALUES('DUGR08085001','Duguay','Roger',null)
/
INSERT INTO Etudiant
VALUES('LAVP08087001','Lavoie','Paul',null)
/
INSERT INTO Etudiant
VALUES('TREY09087501','Tremblay','Yvon',7316)
/
INSERT INTO Inscription
VALUES('TREJ18088001','INF1110',20,32003,'16/08/2003',null,80,null)
/
INSERT INTO Inscription
VALUES('LAVP08087001','INF1110',20,32003,'16/08/2003',null,80,null)
/
INSERT INTO Inscription
VALUES('TREL14027801','INF1110',30,32003,'17/08/2003',null,90,null)
/
INSERT INTO Inscription
VALUES('MARA25087501','INF1110',20,32003,'20/08/2003',null,80,null)
/
INSERT INTO Inscription
VALUES('STEG03106901','INF1110',20,32003,'17/08/2003',null,70,null)
/
INSERT INTO Inscription
VALUES('TREJ18088001','INF1130',10,32003,'16/08/2003',null,70,null)
/
INSERT INTO Inscription
VALUES('TREL14027801','INF1130',30,32003,'17/08/2003',null,80,null)
/
INSERT INTO Inscription
VALUES('MARA25087501','INF1130',10,32003,'22/08/2003',null,90,null)
/
INSERT INTO Inscription
VALUES('DEGE10027801','INF3180',30,32003,'16/08/2003',null,90,null)
/
INSERT INTO Inscription
VALUES('MONC05127201','INF3180',30,32003,'19/08/2003',null,60,null)
/
INSERT INTO Inscription
VALUES('VANV05127201','INF3180',30,32003,'16/08/2003','20/09/2003',null,null)
/
INSERT INTO Inscription
VALUES('EMEK10106501','INF3180',40,32003,'19/08/2003',null,80,null)
/
INSERT INTO Inscription
VALUES('DUGR08085001','INF3180',40,32003,'19/08/2003',null,70,null)
/
INSERT INTO Inscription
VALUES('TREJ18088001','INF2110',10,12004,'19/12/2003',null,80,null)
/
INSERT INTO Inscription
VALUES('TREL14027801','INF2110',10,12004,'20/12/2003',null,90,null)
/
INSERT INTO Inscription
VALUES('MARA25087501','INF2110',40,12004,'19/12/2003',null,90,null)
/
INSERT INTO Inscription
VALUES('STEG03106901','INF2110',40,12004, '10/12/2003',null,70,null)
/
INSERT INTO Inscription
VALUES('VANV05127201','INF3180',10,12004, '18/12/2003',null,90,null)
/
INSERT INTO Inscription
VALUES('DEGE10027801','INF5180',10,12004, '15/12/2003',null,90,null)
/
INSERT INTO Inscription
VALUES('MONC05127201','INF5180',10,12004, '19/12/2003','22/01/2004',null,null)
/
INSERT INTO Inscription
VALUES('EMEK10106501','INF5180',40,12004, '19/12/2003',null,80,null)
/
INSERT INTO Inscription
VALUES('DUGR08085001','INF5180',10,12004, '19/12/2003',null,80,null)
/

/*
 * MISE � JOUR DES COTES POUR SATISFAIRE C7 - UTILISE LA FONCTION PLSQL fCotePourNote(note) et retourne une COTE
 * TELLE QUE A,B,C,D OU E.
 */

UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'TREJ18088001' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'LAVP08087001' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'TREL14027801' and note = 90
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'MARA25087501' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'STEG03106901' and note = 70
/
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'TREJ18088001' and note = 70
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'TREL14027801' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'MARA25087501' and note = 90
/
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'DEGE10027801' and note = 90
/
UPDATE Inscription SET cote = fCotePourNote(60) where codePermanent = 'MONC05127201' and note = 60
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'EMEK10106501' and note = 80
/ 
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'DUGR08085001' and note = 70
/ 
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'TREJ18088001' and note = 80
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'TREL14027801' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'MARA25087501' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'STEG03106901' and note = 70
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'VANV05127201' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'DEGE10027801' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'EMEK10106501' and note = 80
/ 
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'DUGR08085001' and note = 80
/ 

SELECT * FROM Inscription
/

COMMIT
/
PROMPT Contenu des tables
SELECT * FROM Cours
/
SELECT * FROM Prealable
/
SELECT * FROM SessionUQAM
/
SELECT * FROM Professeur
/
SELECT * FROM GroupeCours
/
SELECT * FROM Etudiant
/
SELECT * FROM Inscription
/

SET ECHO OFF
SPOOL OFF