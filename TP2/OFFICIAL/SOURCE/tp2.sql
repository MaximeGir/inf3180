-- Maxime Girard
-- GIRM30058500
-- INF3180
-- GROUPE 30
-- SESSION AUTOMNE 2012

SET ECHO ON
SET SERVEROUTPUT ON 
SPOOL tp2.out

-- IMPL�MENTATION DES CONTRAINTES D'INT�GRIT� DANS LE SCH�MA EXISTANT.	LES TESTS SONT EN DERNIER. 

-- C1

ALTER TABLE COURS
ADD CONSTRAINT C1
CHECK (REGEXP_LIKE(SIGLE,'[A-Z][A-Z][A-Z][0-9][0-9][0-9][0-9]'))
/

--C2

ALTER TABLE COURS
ADD CONSTRAINT C2 
CHECK (nbCredits BETWEEN 0 AND 99)
/

--C3

ALTER TABLE SessionUQAM
ADD CONSTRAINT C3 
CHECK(dateFin >= dateDebut + 90)
/

--C4

ALTER TABLE Inscription
ADD CONSTRAINT C4
CHECK (dateAbandon IS NULL OR note IS NULL)
/

--C5

ALTER TABLE Inscription
DROP CONSTRAINT CECodePermamentRefEtudiant
/

--C5

ALTER TABLE Inscription
ADD CONSTRAINT CECodePermamentRefEtudiant
FOREIGN KEY (codePermanent) 
REFERENCES Etudiant(codePermanent) 
ON DELETE CASCADE
/

--C6 CREATION DU TRIGGER

CREATE OR REPLACE TRIGGER C6
BEFORE UPDATE OF note ON Inscription
REFERENCING
	OLD AS vieilleNote
	NEW AS nouvelleNote
FOR EACH ROW
WHEN (abs(vieilleNote.note - nouvelleNote.note) > 20)
BEGIN
  RAISE_APPLICATION_ERROR(-20001, '�cart de note est trop grand.');
END;
/

--C7

ALTER TABLE Inscription 
ADD Cote CHAR(1)
/

ALTER TABLE Inscription 
ADD CONSTRAINT C7 
CHECK (cote IN ('A', 'B', 'C', 'D','E') or cote IS NULL)
/

--C7 (FONCTION PLSQL)

CREATE OR REPLACE FUNCTION fCotePourNote (noteToConvert Inscription.note%TYPE)
RETURN Inscription.cote%TYPE IS coteConvertie Inscription.cote%TYPE;
BEGIN
IF (noteToConvert BETWEEN 0 AND 59) THEN coteConvertie:='E' ;
ELSIF (noteToConvert BETWEEN 60 AND 69) THEN coteConvertie:='D' ;
ELSIF (noteToConvert BETWEEN 70 AND 79) THEN coteConvertie:='C' ;
ELSIF (noteToConvert BETWEEN 80 AND 89) THEN coteConvertie:='B' ;
ELSIF (noteToConvert BETWEEN 90 AND 100) THEN coteConvertie:='A' ;
ELSIF (noteToConvert IS NULL) THEN coteConvertie:= NULL ;
END IF;
RETURN coteConvertie;
END;
/ 


--PROCEDURE PBULLETIN. 
--
--
CREATE OR REPLACE PROCEDURE pBulletin (cPermanent Inscription.codePermanent%TYPE) 
IS

nomEtudiant varchar2(15);
prenomEtudiant varchar2(15);
sigleEtudiant varchar2(7);
noGroupeEtudiant number;
noSessionEtudiant number;
noteEtudiant number;
coteEtudiant char(1);

CURSOR curseur(cPermanent Inscription.codePermanent%TYPE) IS
SELECT  sigle,noGroupe, codeSession, note, cote
FROM Inscription
WHERE codePermanent = cPermanent;

BEGIN

DBMS_OUTPUT.PUT('CODE PERMANENT : ');
DBMS_OUTPUT.PUT_LINE(cPermanent);

select nom 
into nomEtudiant
from etudiant 
where codePermanent = cPermanent;

DBMS_OUTPUT.PUT('NOM : ');
DBMS_OUTPUT.PUT_LINE(nomEtudiant);

select prenom
into prenomEtudiant
from etudiant
where codePermanent = cPermanent;

DBMS_OUTPUT.PUT('PRENOM : ');
DBMS_OUTPUT.PUT_LINE(prenomEtudiant);

OPEN curseur(cPermanent);
LOOP

FETCH curseur INTO sigleEtudiant, noGroupeEtudiant, noSessionEtudiant, noteEtudiant, coteEtudiant;
EXIT WHEN curseur%NOTFOUND;

DBMS_OUTPUT.PUT('SIGLE : ');
DBMS_OUTPUT.PUT(sigleEtudiant);

DBMS_OUTPUT.PUT('NOGROUPE : ');
DBMS_OUTPUT.PUT_LINE(noGroupeEtudiant);

DBMS_OUTPUT.PUT('SESSION : ');
DBMS_OUTPUT.PUT_LINE(noSessionEtudiant);

DBMS_OUTPUT.PUT('NOTE : ');
DBMS_OUTPUT.PUT_LINE(noteEtudiant);

DBMS_OUTPUT.PUT('COTE : ');
DBMS_OUTPUT.PUT_LINE(coteEtudiant);

END LOOP;
CLOSE curseur;

END pBulletin;
/
--FIN PROCEDURE PBULLETIN
 
-- VUE MOYENNEPARGROUPE
 
CREATE OR REPLACE VIEW MoyenneParGroupe AS
SELECT DISTINCT Sigle, noGroupe, codeSession, avg(note) as moyenneNote
FROM Inscription
GROUP BY noGroupe,Sigle, codeSession
/

SELECT * FROM MoyenneParGroupe
/

/*
 * MISE A JOUR DE LA TABLE INSCRIPTIONS (NOTES)
 */
 
 /*
 * MISE � JOUR DES COTES POUR SATISFAIRE C7 - UTILISE LA FONCTION PLSQL fCotePourNote(note) et retourne une COTE
 * TELLE QUE A,B,C,D OU E.
 */

UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'TREJ18088001' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'LAVP08087001' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'TREL14027801' and note = 90
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'MARA25087501' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'STEG03106901' and note = 70
/
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'TREJ18088001' and note = 70
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'TREL14027801' and note = 80
/
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'MARA25087501' and note = 90
/
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'DEGE10027801' and note = 90
/
UPDATE Inscription SET cote = fCotePourNote(60) where codePermanent = 'MONC05127201' and note = 60
/
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'EMEK10106501' and note = 80
/ 
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'DUGR08085001' and note = 70
/ 
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'TREJ18088001' and note = 80
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'TREL14027801' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'MARA25087501' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(70) where codePermanent = 'STEG03106901' and note = 70
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'VANV05127201' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(90) where codePermanent = 'DEGE10027801' and note = 90
/ 
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'EMEK10106501' and note = 80
/ 
UPDATE Inscription SET cote = fCotePourNote(80) where codePermanent = 'DUGR08085001' and note = 80
/ 

-- TEST DE CONTRAINTES D`INTEGRITES C1 JUSQU`A C6

--TEST C1

-- TEST C1 INSERTION DE COURS DONT LE SIGLE NE RESPECTE PAS LE PATRON DE CARACTERE.
-- SIGLE CONTIENT 4 LETTRE ET SEULEMENT 3 CHIFFRES

INSERT INTO Cours 
VALUES('INF318A','Fichiers et bases de donnees',3)
/

-- SIGLE DONT LE NOMBRE DE CHIFFRE NE RESPECTENT PAS LE PATRON DE CARACTERE.
INSERT INTO Cours 
VALUES('INF318','Fichiers et bases de donnees',3)
/

--TEST C2

-- INSERTION DE COURS DONT LE NOMBRE DE CR�DIT NE RESPECTENT PAS LA CONTRAINTE.
-- INSERTION D�PASSANT LA LIMITE SUP�RIEUR DU NBCR�DIT SPECIFI� DANS CONTRAINTE.
INSERT INTO Cours 
VALUES('INF2110','Programmation II',333)
/

-- INSERTION D�PASSANT LA LIMITE INF�RIEUR DU NBCR�DIT SPECIFI� DANS CONTRAINTE.

INSERT INTO Cours 
VALUES('INF3123','Programmation objet',-20)
/

-- TEST C3

-- INSERTION DE DATE DE FIN DE SSION QUI N'EST AS AU MINIMUM 90 JOURS APRES LA DATE DE 
-- DEBUT DE SESSION

INSERT INTO SessionUQAM
VALUES(32003,'3/09/2003','17/10/2003')
/

INSERT INTO SessionUQAM
VALUES(12004,'8/01/2004','2/02/2004')
/

-- TEST C4
-- INSERTION DE DATE D'ABANDON NULL ET DE NOTE NULL 

INSERT INTO Inscription
VALUES('DEGE10027801','INF5180',10,12004, '15/12/2003',NULL,NULL)
/

-- INSERTION DE DATE D'ABANDON NON NULL ET DE NOTE NON NULL

INSERT INTO Inscription
VALUES('MONC05127201','INF5180',10,12004, '19/12/2003','22/01/2004',99)
/

-- TEST C5
-- SUPPRESSION D'UN �TUDIANT AINSI QUE TOUTES SES INSCRIPTIONS 

-- SUPRESSION DE JEAN TREMBLAY DANS LA TABLE �TUDIANT ET DISPARITION DE SES INSCRIPTIONS 
-- DANS LA TABLE INSCRIPTION

-- VOICI JEAN TREMBLAY DANS LA TABLE ETUDIANT

SELECT * FROM ETUDIANT
WHERE CODEPERMANENT = 'TREJ18088001'
/

-- VOICI LES INSCRIPTIONS DE JEAN TREMBLAY DANS LA TABLE INSCRIPTION.

SELECT * FROM INSCRIPTION
WHERE CODEPERMANENT = 'TREJ18088001'
/


-- SUPPRESSION DE JEAN TREMBLAY EN TANT QU'�TUDIANT

DELETE FROM ETUDIANT 
WHERE CODEPERMANENT = 'TREJ18088001'
/


-- AUCUNE INSCRIPTION NE SUBSISTE � LA SUPPRESSION.

SELECT * FROM INSCRIPTION
WHERE CODEPERMANENT = 'TREJ18088001'
/


-- TEST C6
-- �CART DE NOTE PLUS PETIT QUE 20 POINTS LORS DE MISE � JOUR. 

-- MISE � JOUR D'UNE NOTE DE PLUS DE 20 POINTS 
-- NOTE INITIAL : 60

UPDATE Inscription
SET note = 100
where codePermanent = 'MONC05127201'
/

-- MISE � JOUR D'UNE NOTE DE MOIN DE 20 POINTS

UPDATE Inscription
SET note = 75
where codePermanent = 'MONC05127201' 
AND Sigle = 'INF3180'
/

SET ECHO OFF
SPOOL OFF
