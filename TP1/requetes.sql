  
    --Fichier       : requetes.sql 
    --Auteur        : Maxime Girard
	--Code Permanent: GIRM300585-00
    --Sigle de cours: INF3180 
	--Remis �       : Ph.D Robert Godin.

SET ECHO ON 
SPOOL requetes.out


ALTER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY'
/

-- 1 Les num�ros des articles (sans r�p�titions) qui ont �t� command�s au moins une fois.
select distinct noArticle
from LigneCommande
/

-- 2 Le num�ro et la description des articles dont le num�ro est sup�rieur � 45
-- et dont la description d�bute par 'Po' ou contient la lettre 'a'.

select noArticle, description
from Article
where noArticle > 45 AND (description like 'Po%' OR description like '%a%')
/

-- 3 Le num�ro et la description des articles 10, 40 , et 80.

select noArticle , description
from Article
where noArticle IN (10, 40, 80)
/

-- 4 Le nom du client de la commande 7.

select nomClient
from Client NATURAL JOIN Commande
where noCommande = 7
/

-- 5 Les noms des clients (sans r�p�titions) qui ont une livraison dat�e du 4 juin ou du 9 juillet 2000.

select distinct nomClient
from Client, Commande, LigneCommande, DetailLivraison, Livraison 
where Client.noClient = Commande.noClient AND
Commande.noCommande = LigneCommande.noCommande AND
LigneCommande.noCommande = DetailLivraison.noCommande AND
Livraison.noLivraison = DetailLivraison.noLivraison AND
(dateLivraison = '04/06/2000' OR dateLivraison = '09/07/2000')
/

-- 6 La liste des dates pour lesquelles il y a au moins une livraison ou une commande.
--   Les r�sultats sont produits en une colonne nomm�e Date�venement.

select distinct dateLivraison as DateEvenement 
from Livraison
union
select distinct dateCommande as DateEvenement
from Commande
/

-- 7 En deux colonnes, donner les noArticle et la quantit� totale command�e de l'article incluant 
-- les articles dont la quantit� totale command�e est �gale � 0.

select noArticle as "numeroArticle", SUM("quantite") as "Quantite Commande"
from (select Article.noArticle, NVL(LigneCommande.quantite, 0) as "quantite"
from Article left join LigneCommande on Article.noArticle = LigneCommande.noArticle 
where quantite >= 0 OR quantite IS NULL
order by LigneCommande.noArticle asc)
group by noArticle
/

-- 8 En deux colonnes, donner les noArticle et la quantit� totale command�e de
-- l'article, incluant les articles dont la quantit� totale command�e est �gale � 0 et 
-- uniquement pour les articles dont le noArticle est inf�rieur � 70 et la quantit�
-- totale command�e est inf�rieure � 5.

select noArticle as "numeroArticle", SUM("quantite") as "Quantite Commande"
from (select Article.noArticle, NVL(LigneCommande.quantite, 0) as "quantite"
from Article left join LigneCommande on Article.noArticle = LigneCommande.noArticle 
where quantite >= 0 OR quantite IS NULL
order by LigneCommande.noArticle asc)
where "quantite" < 5 AND noArticle < 70
group by noArticle
/

-- 9 Le noLivraison, noCommande, noArticle, la date de la commande, la quantit� command�, la date de la livraison,
-- la quantit� livr�e, et le nombre de jours �coul�s entre la commande et la livraison dans le cas o� ce nombre � 
-- d�pass� 2 jours et le nombre de jours �coul�s depuis la commande jusqu'� aujourd'hui est sup�rieur � 100

select Livraison.noLivraison, DetailLivraison.noCommande, LigneCommande.noArticle, 
Commande.dateCommande, LigneCommande.quantite, Livraison.dateLivraison,
DetailLivraison.quantiteLivree, (Livraison.dateLivraison - Commande.dateCommande) as nombreDeJour
from Commande, LigneCommande, Livraison, DetailLivraison
where LigneCommande.noArticle =  DetailLivraison.noArticle AND DetailLivraison.noLivraison = Livraison.noLivraison AND LigneCommande.noCommande = Commande.noCommande
 and ((Livraison.dateLivraison - Commande.dateCommande > 2 and SYSDATE - Commande.dateCommande > 100))
/

-- 10 La table D�tailLivraison tri�e en ordre d�croissant de noCommande et pour chaque noCommande , en ordre croissant
-- de quantit�Livr�e.

select * from DetailLivraison 
order by noCommande desc, quantiteLivree asc
/ 

-- 11 Le nombre d'articles dont la quantit� totale command�e est sup�rieure � 5 et le nombre d'articles dont la quantit� totale command�e
-- est inf�rieure � 2 (en deux colonnes).

select noArticle, quantiteTotale
from (select noArticle, sum(quantite) as quantiteTotale
from LigneCommande
group by noArticle
order by noArticle ASC)
where quantiteTotale < 2 or quantiteTotale > 5
/

-- 12 Les noArticle des articles qui n'ont jamais �t� command�s

(select noArticle
from Article)
minus
(select noArticle
from LigneCommande)
/

-- 13 Le noLivraison des derni�res livraisons (i.e. celles dont la date de livraison est la plus r�cente).

select noLivraison
from Livraison
where dateLivraison = (select max(dateLivraison) from Livraison)
/

-- 14 Le montant total command� pour chaque paire (noClient, noArticle) dans les cas o� le montant d�passe 50$

select Client.noClient as noClient , LigneCommande.noArticle as noArticle, Article.prixUnitaire*LigneCommande.quantite as total
from Client, Commande, LigneCommande, Article
where Client.noClient = Commande.noClient and Commande.noCommande = LigneCommande.noCommande and LigneCommande.noArticle = Article.noArticle
and (Article.prixUnitaire*LigneCommande.quantite > 50.00)
order by noArticle, noClient asc
/

-- 15 Les noLivraison des livraisons qui touchent � toutes et chacune des commandes du client 
--    10 faites au mois de juin 2000.

select Livraison.noLivraison
from Commande, DetailLivraison, Livraison
where Commande.noCommande = DetailLivraison.noCommande 
and DetailLivraison.noLivraison = Livraison.noLivraison 
and Commande.noClient = 10 
and (Livraison.dateLivraison >= '01-06-2000' and Livraison.dateLivraison <= '30-06-2000')
group by Livraison.noLivraison
/

SET ECHO OFF
SPOOL OFF